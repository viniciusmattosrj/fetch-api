# API NODE

Consultando API de CEP com Fetch API, o novo AJAX. - <a href="https://www.youtube.com/watch?v=Pi6wkdU2vR4">API Via CEP</a> - Instrutor


## Requerimentos

- NPM >= 6.4

- Node >= 10


## Instalação

Navege até dentro da pasta projetos e realize o git clone do projeto
```bash
https://gitlab.com/viniciusmattosrj/fetch-api
```

Para que o git não considere alterações de permissão como modificações a serem rastreadas, execute:
```
git config core.fileMode false
```

Entre pelo terminal na pasta do projeto e execute:
```
npm init
npm install -y
npm install bootstrap --save
```


## Contribuições
Caso identifique pontos
que possam ser aprimorados envie o seu PR. ;-)


## License
[MIT](https://choosealicense.com/licenses/mit/)
